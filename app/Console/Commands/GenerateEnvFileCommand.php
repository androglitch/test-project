<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateEnvFileCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:environment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(file_exists(base_path() . '/.env'))
            return 0;
        $path = fopen(base_path() . '/.env', 'w');
        foreach($_ENV as $key => $value){
            if(strpos($key, 'APP_ENV_') === false)
                continue;
            $finalKey = str_replace('APP_ENV_', '', $key);
            fwrite($path, $finalKey. "=\"" . $value ."\"\n");
        }
        fclose($path);
        return 0;
    }
}
