@servers(['localhost' => '127.0.0.1'])

@story('deploy')
git_clone
install_composer
generate_env
@endstory


@task('git_clone')
echo "Clone"
@endtask

@task('install_composer')
echo "Install"
@endtask

@task('generate_env')
echo "Generate"
@endtask